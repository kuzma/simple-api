import http from 'http';
import anyTest, { TestInterface } from 'ava';
import got from 'got';
import testListen from 'test-listen';
import getApp from './app';
import connect from './db';
import { createLogger } from './utils/logger';

const logger = createLogger('app.spec.ts');

const test = anyTest as TestInterface<{
  server: http.Server;
  prefixUrl: string;
}>;

test.before(async (t) => {
  try {
    await connect();
    t.context.server = http.createServer(getApp()); // eslint-disable-line no-param-reassign
    t.context.prefixUrl = await testListen(t.context.server); // eslint-disable-line no-param-reassign
  } catch (err) {
    logger.error(err, 'failed to initialize app for tests');
  }
});

test.after.always((t) => {
  t.context.server.close();
});

const mocks = { email: 'fake@test.com', message: 'hello, this is a test message' };

test.serial('create and get invites', async (t) => {
  const body = await got('api/invite/123', {
    prefixUrl: t.context.prefixUrl,
  }).json();
  t.deepEqual(body, { success: false });

  const body2: any = await got
    .post('api/invite', {
      prefixUrl: t.context.prefixUrl,
      json: mocks,
    })
    .json();
  t.is(body2.success, true);

  const { inviteCode }: { inviteCode: string } = body2;

  const body3 = await got(`api/invite/${inviteCode}`, {
    prefixUrl: t.context.prefixUrl,
  }).json();
  t.deepEqual(body3, mocks);
});
