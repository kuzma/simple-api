import pino from 'pino';
import config from 'config';

declare module 'pino' {
  interface LogFn {
    (msg: string, ...args: any[]): Promise<void>;
    (obj: object, msg?: string, ...args: any[]): Promise<void>;
  }
}

export type NodeLogger = pino.Logger & {
  final: () => pino.Logger;
};

/**
 * @typedef {Object} IllumeLogger
 * @property {Function} trace
 * @property {Function} debug
 * @property {Function} info
 * @property {Function} warn
 * @property {Function} error
 * @property {Function} fatal
 */

/**
 * @return {IllumeLogger} logger
 */
export default function createLogger(moduleName?: string, extra = {}, extreme = false): NodeLogger {
  const opts = {
    ...extra,
    name: moduleName || 'unset',
    level: process.env.LOGLEVEL || (config.has('log_level') && config.get('log_level')) || 'debug',
    timestamp: () => `,"timestamp":"${new Date().toISOString()}"`,
  };

  const logger = extreme ? pino(opts, pino.extreme()) : pino(opts);

  // Returns a logger instance that is guaranteed to write synchronously
  // Safe equivalent to `pino.final(logger)`
  logger.final = () => {
    try {
      const final = pino.final(logger) as any;
      return final;
    } catch (e) {
      return logger;
    }
  };

  return logger as NodeLogger;
}
