import { MigrationInterface, QueryRunner } from 'typeorm';

export class initial1628718083163 implements MigrationInterface {
  name = 'initial1628718083163';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER ROLE root SET search_path TO public`);
    await queryRunner.query(
      `CREATE TABLE "card_invite" ("id" SERIAL NOT NULL, "invite_code" character varying(36) NOT NULL, "email" character varying(254) NOT NULL, "message" character varying(500), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL, CONSTRAINT "UQ_52d4399407f835eea1b08438c2f" UNIQUE ("invite_code"), CONSTRAINT "PK_cb3f8e51f823b2ef8edd8d4c522" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "profile" ("id" SERIAL NOT NULL, "email" character varying(254) NOT NULL, "first_name" character varying(30) NOT NULL DEFAULT '', "last_name" character varying(150) NOT NULL DEFAULT '', CONSTRAINT "PK_3dd8bfc97e4a77c70971591bdcb" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "profile"`);
    await queryRunner.query(`DROP TABLE "card_invite"`);
  }
}
