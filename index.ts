import http from 'http';
import withShutdown from 'http-shutdown';
import config from 'config';
import getApp from './src/app';
import connect from './src/db';

import * as myLogger from './src/utils/logger';

const logger = myLogger.createLogger();

// connect to the DB, then start up the server
connect().then(() => {
  // Server implementation details/configuration below here.
  const port = config.get('server.port');
  const server = withShutdown(http.createServer(getApp())).listen(port);

  logger.info(`server listening on port ${port}`);
  process.on('SIGTERM', () => {
    logger.info('shutdown requested');
    server.shutdown(() => {
      logger.info('shutdown complete');
      process.exit(0);
    });
  });
});
