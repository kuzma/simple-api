import { RequestHandler } from 'express';
import CardInvite from '../../entity/CardInvite';
import { createLogger } from '../../utils/logger';

const logger = createLogger('invite.controller');

export const createInvite: RequestHandler = async (req, res) => {
  try {
    const { email, message }: { email: string; message: string } = req.body;
    const invite = new CardInvite(email, message);
    await invite.save();
    return res.json({ success: true, inviteCode: invite.inviteCode });
  } catch (err) {
    logger.error({ err }, 'Failed to create invite');
    return res.json({ success: false });
  }
};

export const getInvite: RequestHandler = async (req, res) => {
  try {
    const { code }: { code?: string } = req.params;
    const invite = await CardInvite.findByCode(code);
    if (!invite) throw Error('Not Found');
    const { email, message } = invite;
    return res.json({ email, message });
  } catch (err) {
    logger.warn({ err }, 'Unable to find invite');
    return res.json({ success: false });
  }
};
