import { createConnection, getConnection } from 'typeorm';
import path from 'path';
import config from 'config';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { createLogger } from './utils/logger';

const logger = createLogger();
const { host, port, username, password, database } = config.get('db');

const dbConf: PostgresConnectionOptions = {
  type: 'postgres',
  host,
  port,
  username,
  password,
  database,
  entities: [path.join(__dirname, '/entity/*.ts')],
  migrations: [path.join(__dirname, '/migration/*.ts')],
  migrationsTableName: 'typeform_migrations',
  synchronize: false,
  logging: false,
};

const connect = async () => {
  try {
    await createConnection(dbConf);

    // run any not yet run migrations here...
    const migrations = await getConnection().runMigrations({ transaction: 'none' });

    if (migrations.length > 0) {
      logger.debug(
        { migrations: migrations.map((m) => m.name) },
        `Completed ${migrations.length} migrations`,
      );
    }
  } catch (err) {
    logger.fatal(err, 'Failed to start the DB connection');
    throw Error('Unable to start the database connection');
  }
};

export default connect;
